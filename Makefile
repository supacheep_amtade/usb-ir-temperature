MCU=atmega168
F_CPU=16000000L
TARGET=main.hex
OBJS=usbdrv/usbdrv.o usbdrv/usbdrvasm.o ds18x20/ds18x20lib.o
CFLAGS=-Wall -Os -DF_CPU=$(F_CPU) -Iusbdrv -Ids18x20 -I. -mmcu=$(MCU)

all: $(TARGET)

flash: $(TARGET)
	avrdude -p $(MCU) -c usbasp -u -U flash:w:$(TARGET)

%.hex: %.elf
	avr-objcopy -j .text -O ihex $< $@

%.elf: %.o $(OBJS)
	avr-gcc $(CFLAGS) -o $@ $?

%.o: %.c
	avr-gcc -c $(CFLAGS) -o $@ $<

%.o: %.S
	avr-gcc $(CFLAGS) -x assembler-with-cpp -c -o $@ $<

clean:
	rm -f $(OBJS)
	rm -f $(TARGET)
	rm -f *~
