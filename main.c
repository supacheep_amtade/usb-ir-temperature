#define SYSCLOCK F_CPU
#include <avr/io.h>
#include <avr/interrupt.h>  /* for sei() */
#include <util/delay.h>     /* for _delay_ms() */
#include <avr/pgmspace.h>   /* required by usbdrv.h */

#include "usbdrv.h"
#include "ds18x20/ds18x20lib.h"

/* -----------------------------  Pulse Interface  ------------------------- */
#define IRpin_PIN PINC
#define IRpin 0

#define MAXPULSE 65000		/* Listen within for 1000 msec */
#define TEMP_INT 400000
#define RESOLUTION 20

static uint16_t pulses[200];	/* pair of high and low */
static uint8_t currentpulse;	/* index for pulses */
static uint8_t totalpulse;
uint8_t activeHigh;

void generate25c(void);
void generatePulse(int pulse, int switchpulse);
/* -------------------------------  USB Interface  ------------------------- */
#define RQ_INIT_READ 0
#define RQ_ON_READ 1				/* bRequest alocate[0...100] */

static uint8_t varTemp = 0;
static float tempInterval = TEMP_INT;

static uint8_t shoot25c_t = 0;
static uint8_t shoot18c_t = 0;
static uint8_t shootClose_t = 0;

static uint8_t recording = 0;
/* ------------------------------------------------------------------------- */
/* -------------------------------  USB Interface  ------------------------- */
/* ------------------------------------------------------------------------- */

usbMsgLen_t usbFunctionSetup(uint8_t data[8])
{
	usbRequest_t *rq = (void *)data;

	if (rq->bRequest == 0){
		usbMsgPtr = (uchar *) &totalpulse;
		return sizeof(usbMsgPtr);
	}
	else if (rq->bRequest >= 1 && rq->bRequest <= 200){
		currentpulse = 0;
		usbMsgPtr = (uchar *) &pulses[rq->bRequest - 1];
		return sizeof(usbMsgPtr);
	}
  else if (rq->bRequest == 201){
		usbMsgPtr = (uchar *) &currentpulse;
		return sizeof(usbMsgPtr);
  }
	else if (rq->bRequest == 202){
		usbMsgPtr = (uchar *) &varTemp;
		return sizeof(usbMsgPtr);
  }
	else if (rq->bRequest == 210){
		shoot25c_t = 1;
		return 0;
	}
	else if (rq->bRequest == 211){
		shoot18c_t = 1;
		return 0;
	}
	else if (rq->bRequest == 212){
		shootClose_t = 1;
		return 0;
	}
	return 0;   /* default for not implemented requests: return no data back to host */
}
/* ------------------------------------------------------------------------- */
/* -----------------------------  Pulse Interface  ------------------------- */
/* ------------------------------------------------------------------------- */
#define TIMER_ENABLE_PWM (TCCR1A |= _BV(COM1A1))
#define TIMER_DISABLE_PWM (TCCR1A &= ~(_BV(COM1A1)))

void bigPulse(int p)
{
	int i;
	for (i = 0; i < p; i++){
		TCCR1A |= _BV(COM1A1);	
  	_delay_us(600);
		TCCR1A &= ~(_BV(COM1A1));
		_delay_us(600);
	}
}

void shootClose(int time)
{
	PORTD = 0b00000000;
	_delay_ms(300);
  usbDeviceDisconnect();
	while(time--){
	TCCR1A |= _BV(COM1A1);	
  _delay_us(9000);
	TCCR1A &= ~(_BV(COM1A1));
	_delay_us(4400);
	bigPulse(3);
	_delay_us(1000);
	bigPulse(7);
	_delay_us(1000);
	bigPulse(5);
	_delay_us(1000);
	bigPulse(1);
	_delay_us(1000);
	bigPulse(26);
	_delay_us(1000);
	bigPulse(1);
	_delay_us(1000);
	bigPulse(2);
	_delay_us(1000);
	}
  usbDeviceConnect();
}
	
void shoot18c(int time)
{
	PORTD = 0b00000000;
	_delay_ms(300);
  usbDeviceDisconnect();
	while(time--){
	TCCR1A |= _BV(COM1A1);	
  _delay_us(9000);
	TCCR1A &= ~(_BV(COM1A1));
	_delay_us(4400);
	bigPulse(3);
	_delay_us(1000);
	bigPulse(2);
	_delay_us(1000);
	bigPulse(5);
	_delay_us(1000);
	bigPulse(5);
	_delay_us(1000);
	bigPulse(1);
	_delay_us(1000);
	bigPulse(25);
	_delay_us(1000);
	bigPulse(1);
	_delay_us(1000);
	bigPulse(1);
	_delay_us(1000);
	bigPulse(2);
	_delay_us(1000);
	}
  usbDeviceConnect();
}

void shoot25c(int time)
{
	PORTD = 0b00000000;
	_delay_ms(300);
  usbDeviceDisconnect();
	while(time--){
	TCCR1A |= _BV(COM1A1);	
  _delay_us(9000);
	TCCR1A &= ~(_BV(COM1A1));
	_delay_us(4600);
	bigPulse(3);
	_delay_us(1000);
	bigPulse(2);
	_delay_us(1000);
	bigPulse(5);
	_delay_us(1000);
	bigPulse(3);
	_delay_us(1000);
	bigPulse(2);
	_delay_us(1000);
	bigPulse(27);
	_delay_us(1000);
	bigPulse(1);
	_delay_us(1000);
	bigPulse(1);
	_delay_us(1000);
	}
  usbDeviceConnect();
}

void pwm_init(int feq)
{
	const uint8_t pwmval = SYSCLOCK / 2000 / feq;
  // Set pin to output
  DDRB = 0b00000010;
  // Timer Top
  ICR1 = pwmval; // (16MHz/1)/ICR1

  OCR1A = pwmval / 3;

  // Using Phase Correct PWM mode
  TCCR1A = (1<<COM1A1) | (0<<COM1A0) | (1<<WGM11);
  TCCR1B = (1<<WGM13) | (1<<CS10);
}

void loop(void){
	uint8_t highpulse, lowpulse;
	highpulse = lowpulse = 0;
	PORTD = 0b00001000;
	while ((IRpin_PIN & (1 << IRpin)) != 0){
		/* pin is hign, but we consider as it low because of active low device */
		lowpulse++;
		_delay_us(RESOLUTION);

		usbPoll();

		if ((lowpulse >= MAXPULSE) && (currentpulse != 0)){
			//printpulses_usb();
			totalpulse = currentpulse;
			currentpulse = 0;
			recording = 0;
			return;
		}
	}
	pulses[currentpulse] = lowpulse;

	usbPoll();

	currentpulse++;		/* continue to anorther */ 

	while ((IRpin_PIN & (1 << IRpin)) == 0){
		recording = 1;
	
		PORTD = 0b00000000;
		/* pin is low active, so we low signal to active */
		highpulse++;	/* count off anorther few microsec */
		_delay_us(RESOLUTION);

		usbPoll();

		if ((highpulse >= MAXPULSE) && (currentpulse != 0)){
			//printpulses_usb();
			totalpulse = currentpulse;
			currentpulse = 0;
			return;
		}
	}
	

	usbPoll();

	pulses[currentpulse] = highpulse;	/* if we dont catch timeout */

	currentpulse++;
	
	PORTD = 0b00001000;
}

/* ------------------------------------------------------------------------- */

int main(void)
{
	DDRD  = 0b00001000;	/* set light */
	PORTD = 0b00001000;  

	DDRC  = 0b00000010;	/* set input */
	PORTC = 0b00000001;

	pwm_init(38);

	ds1820_init(DS1820_pin_po);

	usbInit();
  
	sei();
	/* enforce re-enumeration, do this while interrupts are disabled! */
  usbDeviceDisconnect();
	PORTD = 0b00000000;
	varTemp = (int) ds1820_read_temp(DS1820_pin_po);
	PORTD = 0b00001000;
	activeHigh = 0;
	tempInterval = TEMP_INT;
	int i;
	for (i = 0; i < 200; i++)
	{
		pulses[i] = 0;
	}

  usbDeviceConnect();

  /* main event loop */
  for(;;)
  {
		if (recording == 0){
 			usbPoll();

			if (shoot25c_t){
				shoot25c(3);
				shoot25c_t = 0;
				PORTD = 0b00001000;
			}
 		
			usbPoll();

			if (shootClose_t){
				shootClose(3);
				shootClose_t = 0;
				PORTD = 0b00001000;
				}

			usbPoll();

			if (shoot18c_t){
				shoot18c(3);
				shoot18c_t = 0;
				PORTD = 0b00001000;
			}
		
			usbPoll();

			if (tempInterval == 0){
				PORTD = 0b00000000;
				varTemp = (int) ds1820_read_temp(DS1820_pin_po);
				tempInterval = TEMP_INT;	
  			usbDeviceConnect();
				PORTD = 0b00001000;
			} else {
				tempInterval--;
			}
	}	else {
			loop();
			usbPoll();
		}
  }

    return 0;
}

/* ------------------------------------------------------------------------- */
