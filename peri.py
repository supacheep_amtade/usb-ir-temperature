from practicum import McuBoard

RQ_SET_LED    = 0
RQ_GET_SWITCH = 1
RQ_GET_LIGHT  = 2

####################################
class PeriBoard(McuBoard):

    ################################
    def setLed(self, led_no, led_val):
        self.usb_write(RQ_SET_LED, index=led_no, value=led_val)

    ################################
    def setLedValue(self, value):
        self.usb_write(RQ_SET_LED, index=2, value=value/4)
        self.usb_write(RQ_SET_LED, index=1, value=value/2)
        self.usb_write(RQ_SET_LED, index=0, value=value)

    ################################
    def getSwitch(self):
        if self.usb_read(RQ_GET_SWITCH, length=1)[0] == 0:
            return False
        else:
            return True

    ################################
    def getLight(self):
        return self.usb_read(RQ_GET_LIGHT, length=1)[0]
